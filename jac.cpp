#include <mpi.h>

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <JacobiProcess.h>
#ifdef CUDA_JAC
#include <CudaSubprocess.h>
#endif
#include <helpers.h>

using namespace std;
using namespace Jacobi;

int main(int argc, char **argv)
{
 int proc_num = 0, N;
 int size, rank;
 double computation_time = 0., alleps;
 int dims[] = {0, 0};
#ifdef CUDA_JAC
 int CudaElemPerThread[] = {0, 0};
 int CudaBlock[] = {0, 0};
#endif

 /// parameters for creating rectangle grid
 MPI::Intracomm LittleWorld;
 MPI::Cartcomm rect;

 MPI::Init(argc, argv);
 MPI::COMM_WORLD.Set_errhandler(MPI::ERRORS_THROW_EXCEPTIONS);
 rank = MPI::COMM_WORLD.Get_rank();
 size = MPI::COMM_WORLD.Get_size();
 proc_num = size;
#ifdef CUDA_JAC
 bool Trust = false;
#endif

 //args parsing
 for(int i = 0; i < argc; ++i)
 {
     if(string(argv[i]) == "-n")
     {
         proc_num = atoi(argv[++i]);
     }
#ifdef CUDA_JAC
     if(string(argv[i]) == "-b")
     {
         CudaBlock[0] = atoi(argv[++i]);
         CudaBlock[1] = atoi(argv[++i]);
     }
     if(string(argv[i]) == "-ept")
     {
         CudaElemPerThread[0] = atoi(argv[++i]);
         CudaElemPerThread[1] = atoi(argv[++i]);
     }
     if(string(argv[i]) == "+thrust")
     {
         Trust = true;
     }
#endif
     if(string(argv[i]) == "-g")
     {
         dims[0] = atoi(argv[++i]);
         dims[1] = atoi(argv[++i]);
     }
     if(string(argv[i]) == "-h")
     {
         if(rank == root_rank)
            usage();
         MPI::Finalize();
         return 0;
     }
 }
#ifdef CUDA_JAC
     if(CudaBlock[0] == 0) CudaBlock[0] = cuda_default_block[0];
     if(CudaBlock[1] == 0) CudaBlock[1] = cuda_default_block[1];
     if(CudaElemPerThread[0] == 0) CudaElemPerThread[0] = cuda_default_maxelems_per_thread[0];
     if(CudaElemPerThread[1] == 0) CudaElemPerThread[1] = cuda_default_maxelems_per_thread[1];
#endif
 N = atoi(argv[argc-1]);
 if(proc_num <= 0)
 {
     if(rank == root_rank)
     {
        cerr << "Incorrect number of process:" << proc_num << endl;
        usage();
     }
     MPI::Finalize();
     return -1;
 }
 size = min(size, proc_num);
 LittleWorld = MPI::COMM_WORLD.Split((rank >= proc_num ? MPI::UNDEFINED : 2), rank);
 if(LittleWorld == MPI::COMM_NULL)
 {
     MPI::Finalize();
     return 0;
 }

 if(dims[0] * dims[1] > size || dims[0]== 0 || dims[1] == 0)
     for (int i = sqrt(size); i > 0; --i)
     {
         if(size % i == 0)
         {
             dims[0] = i;
             dims[1] = size / i;
             break;
         }
     }
 rect = LittleWorld.Create_cart(2, dims, periods, reorder);
 const int blocksize[] = { (N - 2) / dims[0] + 2, (N - 2) / dims[1] + 2 };
 JacobiProcess *current;
#ifdef CUDA_JAC
    int CudaGrid[] = {0, 0};

    CudaGrid[0] = (blocksize[0] - 2) / (CudaElemPerThread[0] * CudaBlock[0]);
    if((blocksize[0] - 2) % (CudaElemPerThread[0] * CudaBlock[0]) != 0 && rank == root_rank)
    {
        cerr << "ERROR! Grid is not full" << endl;
        MPI::Finalize();
        return -1;
    }

    CudaGrid[1] = (blocksize[1] - 2) / (CudaElemPerThread[1] * CudaBlock[1]);
    if((blocksize[1] - 2) % (CudaElemPerThread[1] * CudaBlock[1]) != 0 && rank == root_rank)
    {
        cerr << "ERROR! Grid is not full" << endl;
        MPI::Finalize();
        return -1;
    }
    current = new CudaSubprocess(rect, dims, rank, blocksize, CudaGrid, CudaBlock, Trust);
#else
    current = new JacobiProcess(rect, dims, rank, blocksize);
#endif

 if(rank == root_rank)
 {
     cerr << "Cart size = [" << dims[0] << ":" << dims[1] << "]" << endl
          << "blocksize = " << blocksize[0] << "><" << blocksize[1] << endl;
#ifdef CUDA_JAC
         cerr << "CUDA size:\n\telem per thread: " << CudaElemPerThread[0] << "><" << CudaElemPerThread[1]
              << "\n\tthreads: " << CudaBlock[0] << "><" << CudaBlock[1]
              << "\n\tblocks: " << CudaGrid[0] << "><" << CudaGrid[1] << endl;
#endif
 }
 //debug
 //cerr << current->matrixes() << endl;

 rect.Barrier();
 computation_time = 0.0;
 if(rank == root_rank)
 {
     computation_time = -MPI::Wtime();
 }
 for(int it=1; it<=itmax; it++)
 {
    alleps = -1.0;
    //barrier must be
    rect.Barrier();
    current->shadowRenew();

    current->relax();
    current->resid();

    //debug
    //cerr << current->matrixes();

    rect.Reduce(&(current->eps), &alleps, 1, MPI::DOUBLE, MPI::MAX, root_rank);
    if(rank == root_rank)
        cerr << "it = " << it << "\teps = " << alleps << endl;
//    if (alleps < maxeps) break;
 }
 /*double curS = 0.0, allS = 0.0;
 curS = current->verify();
 rect.Reduce(&curS, &allS, 1, MPI::DOUBLE, MPI::SUM, root_rank);*/

 if(rank == root_rank)
 {
   computation_time += MPI::Wtime();
   cout /*<< "S = " << allS << endl*/ << "Time: " << computation_time << endl;
 }

 delete current;

 MPI::Finalize();
 return 0;
}
