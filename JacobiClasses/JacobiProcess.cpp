#include "JacobiProcess.h"
#include "helpers.h"

#include <cmath>
#include <cstring>

//debug
#include <sstream>
#include <iomanip>

using namespace std;

namespace Jacobi
{

JacobiProcess::JacobiProcess(const MPI::Cartcomm &comm,
                             const int *this_dims,
                             int big_rank,
                             const int *blocks
                            ) :
    Rect(comm), rank(big_rank), BlockSize(blocks),
    up(-1), down(-1), left(-1), right(-1)
{
    dims = new int[numdims];
    for (int i = 0; i < numdims; ++i)
        dims[i] = this_dims[i];
    Rect.Get_coords(rank, numdims, coords);
    A = new double [BlockSize[0] * BlockSize[1]];
    B = new double [BlockSize[0] * BlockSize[1]];
    UpShape    = new double [BlockSize[1] - 2];
    memset(UpShape, 0, sizeof(double) * (BlockSize[1] - 2));
    DownShape  = new double [BlockSize[1] - 2];
    memset(DownShape, 0, sizeof(double) * (BlockSize[1] - 2));
    LeftShape  = new double [BlockSize[0] - 2];
    memset(LeftShape, 0, sizeof(double) * (BlockSize[0] - 2));
    RightShape = new double [BlockSize[0] - 2];
    memset(RightShape, 0, sizeof(double) * (BlockSize[0] - 2));
    // init
    for(int i=0; i<BlockSize[0]; ++i)
        for(int j=0; j<BlockSize[1]; ++j)
        {
            if(i==0 || i==BlockSize[0]-1 || j==0 || j==BlockSize[1]-1)
                A[i * BlockSize[1] + j] = 0.;
            else
                A[i * BlockSize[1] + j] = ( 4. + i + j +
                                            ((BlockSize[0]-2) * coords[0] +
                                             (BlockSize[1]-2) * coords[1]
                                            )
                                          );
        }
    memset(B, 0, BlockSize[0] * BlockSize[1] * sizeof(double));
    // init neighbours ranks;
    getUpNeighbour();
    getDownNeighbour();
    getRightNeighbour();
    getLeftNeighbour();
}

JacobiProcess::~JacobiProcess()
{
    delete [] A;
    delete [] B;

    delShape(UpShape);
    delShape(DownShape);
    delShape(LeftShape);
    delShape(RightShape);
}

void JacobiProcess::updateShadowsFromLocal()
{
    if(up != -1)
        for(int i = 0; i < BlockSize[1] - 2; ++i)
            UpShape[i] = A[i + 1 + BlockSize[1]];
    if(down != -1)
        for(int i = 0; i < BlockSize[1] - 2; ++i)
            DownShape[i] = A[i + (BlockSize[0] - 2) * BlockSize[1] + 1];
    if(left != -1)
        for(int i = 0; i < BlockSize[0] - 2; ++i)
            LeftShape[i] = A[i * BlockSize[1] + 1 + BlockSize[1]];
    if(right != -1)
        for(int i = 0; i < BlockSize[0] - 2; ++i)
            RightShape[i] = A[i * BlockSize[1] + 2 * (BlockSize[1] - 1)];
}

void JacobiProcess::updateLocalFromShadows()
{
    if(up != -1)
        for(int i = 0; i < BlockSize[1] - 2; ++i)
            A[i + 1] = UpShape[i];
    if(down != -1)
        for(int i = 0; i < BlockSize[1] - 2; ++i)
            A[i + BlockSize[1] * (BlockSize[0] - 1) + 1] = DownShape[i];
    if(left != -1)
        for(int i = 0; i < BlockSize[0] - 2; ++i)
            A[(i + 1) * BlockSize[1]] = LeftShape[i];
    if(right != -1)
        for(int i = 0; i < BlockSize[0] - 2; ++i)
            A[i * BlockSize[1] - 1 + 2 * BlockSize[1]] = RightShape[i];
}

void JacobiProcess::shadowRenew()
{
    updateShadowsFromLocal();

    //exchange
    double *ExchangeBuffer1, *ExchangeBuffer2;
    //up-down
    if(up == -1 && down == -1)
    {
        //nothing to send
        ;
    }
    else if(up >= 0 && down >=0)
    {
        //central sendrecv
        ExchangeBuffer1 = newShape(BlockSize[1] - 2);
        ExchangeBuffer2 = newShape(BlockSize[1] - 2);
        Rect.Sendrecv(UpShape,
                      BlockSize[1] - 2,
                      MPI::DOUBLE,
                      up,
                      upSendTag,
                      ExchangeBuffer1,
                      BlockSize[1] - 2,
                      MPI::DOUBLE,
                      down,
                      dnRecvTag
                     );
        Rect.Sendrecv(DownShape,
                      BlockSize[1] - 2,
                      MPI::DOUBLE,
                      down,
                      dnSendTag,
                      ExchangeBuffer2,
                      BlockSize[1] - 2,
                      MPI::DOUBLE,
                      up,
                      upRecvTag
                    );
        delShape(UpShape);
        delShape(DownShape);
        UpShape = ExchangeBuffer2;
        DownShape = ExchangeBuffer1;
    }
    else if(up == -1)
    {
        ExchangeBuffer1 = newShape(BlockSize[1] - 2);
        Rect.Sendrecv(DownShape,
                      BlockSize[1] - 2,
                      MPI::DOUBLE,
                      down,
                      dnSendTag,
                      ExchangeBuffer1,
                      BlockSize[1] - 2,
                      MPI::DOUBLE,
                      down,
                      dnRecvTag
                     );
        delShape(DownShape);
        DownShape = ExchangeBuffer1;
    }
    else // down == -1
    {
      ExchangeBuffer2 = newShape(BlockSize[1] - 2);
      Rect.Sendrecv(UpShape,
                    BlockSize[1] - 2,
                    MPI::DOUBLE,
                    up,
                    upSendTag,
                    ExchangeBuffer2,
                    BlockSize[1] - 2,
                    MPI::DOUBLE,
                    up,
                    upRecvTag
                   );
      delShape(UpShape);
      UpShape = ExchangeBuffer2;
    }
    //left-right
    if(left == -1 && right == -1)
    {
        //nothing to send
        ;
    }
    else if(left >= 0 && right >=0)
    {
        //central sendrecv
        ExchangeBuffer1 = newShape(BlockSize[0] - 2);
        ExchangeBuffer2 = newShape(BlockSize[0] - 2);
        Rect.Sendrecv(LeftShape,
                      BlockSize[0] - 2,
                      MPI::DOUBLE,
                      left,
                      ltSendTag,
                      ExchangeBuffer1,
                      BlockSize[0] - 2,
                      MPI::DOUBLE,
                      right,
                      rtRecvTag
                     );
        Rect.Sendrecv(RightShape,
                      BlockSize[0] - 2,
                      MPI::DOUBLE,
                      right,
                      rtSendTag,
                      ExchangeBuffer2,
                      BlockSize[0] - 2,
                      MPI::DOUBLE,
                      left,
                      ltRecvTag
                    );
        delShape(LeftShape);
        delShape(RightShape);
        LeftShape = ExchangeBuffer2;
        RightShape = ExchangeBuffer1;
    }
    else if(left == -1)
    {
        ExchangeBuffer1 = newShape(BlockSize[0] - 2);
        Rect.Sendrecv(RightShape,
                      BlockSize[0] - 2,
                      MPI::DOUBLE,
                      right,
                      rtSendTag,
                      ExchangeBuffer1,
                      BlockSize[0] - 2,
                      MPI::DOUBLE,
                      right,
                      rtRecvTag
                     );
        delShape(RightShape);
        RightShape = ExchangeBuffer1;
    }
    else // right == -1
    {
      ExchangeBuffer2 = newShape(BlockSize[0] - 2);
      Rect.Sendrecv(LeftShape,
                    BlockSize[0] - 2,
                    MPI::DOUBLE,
                    left,
                    ltSendTag,
                    ExchangeBuffer2,
                    BlockSize[0] - 2,
                    MPI::DOUBLE,
                    left,
                    ltRecvTag
                   );
      delShape(LeftShape);
      LeftShape = ExchangeBuffer2;
    }

    updateLocalFromShadows();
    eps = 0.0;
}

void JacobiProcess::updateNeighbour(bool isUpLf, bool isUpDn, int &cur)
{
    int bound = 0;
    int neigh = 0;
    if(!isUpDn)
        neigh = 1;
    if(!isUpLf)
        bound = dims[neigh] - 1;

    int temp_coord[] = {-1, -1};
    if(coords[neigh] != bound)
    {
        if(isUpLf)
            temp_coord[neigh] = coords[neigh] - 1;
        else
            temp_coord[neigh] = coords[neigh] + 1;
        temp_coord[1-neigh] = coords[1-neigh];
        cur = Rect.Get_cart_rank(temp_coord);
    }
}

int JacobiProcess::getUpNeighbour()
{
    if(up < 0)
        updateNeighbour(true, true, up);
    return up;
}

int JacobiProcess::getDownNeighbour()
{
    if(down < 0)
        updateNeighbour(false, true, down);
    return down;
}

int JacobiProcess::getLeftNeighbour()
{
    if(left < 0)
        updateNeighbour(true, false, left);
    return left;
}

int JacobiProcess::getRightNeighbour()
{
    if(right < 0)
        updateNeighbour(false, false, right);
    return right;
}

const string JacobiProcess::matrixes()
{
    stringstream locA, locB;
    locA << "[" << coords[0] << ":" << coords[1] << "]\tA:\n";
    locB << coords[0] << ":" << coords[1] << "\tB:\n";
    for(int i = 0; i < BlockSize[0]; ++i)
    {
     for(int j = 0; j < BlockSize[1]; ++j)
     {
         locA << setw(3) << A[i * BlockSize[1] + j] << " ";
         locB << setw(3) << B[i * BlockSize[1] + j] << " ";
     }
     locA << endl;
     locB << endl;
    }
    return locA.str() + locB.str();
}

void JacobiProcess::relax()
{
    for(int i = 1; i <= BlockSize[0] - 2; ++i)
        for(int j = 1; j <= BlockSize[1] - 2; ++j)
        {
            B[i * BlockSize[1] + j] = ( A[(i-1) * BlockSize[1] + j] +
                                        A[(i+1) * BlockSize[1] + j] +
                                        A[i * BlockSize[1] + j-1] +
                                        A[i * BlockSize[1] + j+1] +
                                        2 * A[i * BlockSize[1] + j]
                                      ) / 6.;
        }
}

void JacobiProcess::resid()
{
    eps = -1.0;
    for(int i = 1; i <= BlockSize[0] - 2; ++i)
        for(int j = 1; j <= BlockSize[1] - 2; ++j)
        {
            double e;
            e = fabs(A[i * BlockSize[1] + j] - B[i * BlockSize[1] + j]);
            A[i * BlockSize[1] + j] = B[i * BlockSize[1] + j];
            eps = max(eps, e);
        }
}

double JacobiProcess::verify()
{
    double s = 0.0;
    for(int i = 1; i <= BlockSize[0] - 2; ++i)
        for(int j = 1; j <= BlockSize[1] - 2; ++j)
        {
            s = s + A[i * BlockSize[1] + j] *
                    (i + 1 + (coords[0] * (BlockSize[0]-2))) *
                    (j + 1 + (coords[1] * (BlockSize[1]-2))) /
                     ( ((BlockSize[0] - 2) * dims[0] + 2) * ((BlockSize[1] - 2) * dims[1] + 2) );
        }

    return s;
}

double * JacobiProcess::newShape(size_t size)
{
    double *tmp = new double [size];
    memset(tmp, -1, size * sizeof(double));
    return tmp;
}

void JacobiProcess::delShape(double *shape)
{
    delete [] shape;
}

const string JacobiProcess::shapes()
{
    stringstream loc;
    loc << "[" << coords[0] << ":" << coords[1] << "]" << endl;

    loc << "\tup:";
    for(int i = 0; i < BlockSize[1] - 2; ++i)
        loc << " " << UpShape[i];
    loc << endl;

    loc << "\tdown:";
    for(int i = 0; i < BlockSize[1] - 2; ++i)
        loc << " " << DownShape[i];
    loc << endl;

    loc << "\tleft:";
    for(int i = 0; i < BlockSize[0] - 2; ++i)
        loc << " " << LeftShape[i];
    loc << endl;

    loc << "\tright:";
    for(int i = 0; i < BlockSize[0] - 2; ++i)
        loc << " " << RightShape[i];
    loc << endl;

    return loc.str();
}

}
