#include <CudaKernels.cuh>

#include <cuda_runtime.h>
#ifdef CUDA_PRINTF
#include <stdio.h>
#endif

__global__ void cuda_sh_relax(double *A, double *B, int matrix0, int matrix1)
{
    extern __shared__ double shared_A[];
    const int blck_sz[] = { (matrix0 - 2) / gridDim.x + 2, (matrix1 - 2) / gridDim.y + 2};
    const int thrd_sz[] = { (blck_sz[0] - 2) / blockDim.x, (blck_sz[1] - 2) / blockDim.y };
    int i_start = (blck_sz[0]-2) * blockIdx.x + thrd_sz[0] * threadIdx.x + 1;
    int j_start = (blck_sz[1]-2) * blockIdx.y + thrd_sz[1] * threadIdx.y + 1;
    double *start_A = A + i_start * matrix1 + j_start;
    double *start_B = B + i_start * matrix1 + j_start;
    double *start_shA = shared_A + (thrd_sz[0] * threadIdx.x + 1) * blck_sz[1] + thrd_sz[1] * threadIdx.y + 1;
    for(int i = 0; i < thrd_sz[0]; ++i)
        for(int j = 0; j < thrd_sz[1]; ++j)
    		*(start_shA + i * blck_sz[1] + j) = *(start_A + i * matrix1 + j);
    if( threadIdx.x == 0 ) // up shape
      for(int j = 0; j < thrd_sz[1]; ++j)
      {
         *(start_shA - blck_sz[1] + j) = *(start_A - matrix1 + j);
      }
    if( threadIdx.y == 0 ) // left shape
      for(int i = 0; i < thrd_sz[0]; ++i)
      {
         *(start_shA - 1 + i * blck_sz[1]) = *(start_A - 1 + i * matrix1);
      }
    if( threadIdx.x == blockDim.x-1 ) // down shape
      for(int j = 0; j < thrd_sz[1]; ++j)
      {
         *(start_shA + blck_sz[1] * thrd_sz[0] + j) = *(start_A + matrix1 * thrd_sz[1] + j);
      }

    if( threadIdx.y == blockDim.y-1 ) // right shape
      for(int i = 0; i < thrd_sz[0]; ++i)
      {
         *(start_shA + thrd_sz[1] + i * blck_sz[1]) = *(start_A + thrd_sz[1] + i * matrix1);
      }
    __syncthreads();
    start_A = start_shA;

//same as global, but matrix1 changed to blck_sz[1]
    for(int i = 0; i < thrd_sz[0]; ++i)
        for(int j = 0; j < thrd_sz[1]; ++j)
{
/*
#ifdef CUDA_PRINTF
printf("B[%d][%d] = (%f + %f + %f + %f + 2*%f) / 6\n", i + i_start, j + j_start,
         *(start_A + (i-1) * blck_sz[1] + j),
         *(start_A + (i+1) * blck_sz[1] + j),
         *(start_A + i * blck_sz[1] + j-1),
         *(start_A + i * blck_sz[1] + j+1),
         *(start_A + i * blck_sz[1] + j));
#endif
*/
            *(start_B + i * matrix1 + j) = ( *(start_A + (i-1) * blck_sz[1] + j) +
                                             *(start_A + (i+1) * blck_sz[1] + j) +
                                             *(start_A + i * blck_sz[1] + (j-1)) +
                                             *(start_A + i * blck_sz[1] + (j+1)) +
                                             2 * *(start_A + i * blck_sz[1] + j)
                                           ) / 6.;
}
}

__global__ void cuda_gl_relax(double *A, double *B, int matrix0, int matrix1)
{
    int blck_sz[] = { (matrix0 - 2) / gridDim.x + 2, (matrix1 - 2) / gridDim.y + 2};
    int thrd_sz[] = { (blck_sz[0] - 2) / blockDim.x, (blck_sz[1] - 2) / blockDim.y };
    int i_start = (blck_sz[0]-2) * blockIdx.x + thrd_sz[0] * threadIdx.x + 1;
    int j_start = (blck_sz[1]-2) * blockIdx.y + thrd_sz[1] * threadIdx.y + 1;
    double *start_A = A + i_start * matrix1 + j_start;
    double *start_B = B + i_start * matrix1 + j_start;
/*
#ifdef CUDA_PRINTF
    printf("t = [%d:%d] on b = [%d:%d]\n"
                    "\tstart:[%d:%d] == %f\n",
            threadIdx.x, threadIdx.y,
            blockIdx.x, blockIdx.y,
            i_start, j_start, *start_A);
#endif
*/
    for(int i = 0; i < thrd_sz[0]; ++i)
        for(int j = 0; j < thrd_sz[1]; ++j)
{
/*
#ifdef CUDA_PRINTF
printf("B[%d][%d] = (%f + %f + %f + %f + 2*%f) / 6\n", i + i_start, j + j_start,
         *(start_A + (i-1) * matrix1 + j),
         *(start_A + (i+1) * matrix1 + j),
         *(start_A + i * matrix1 + j-1),
         *(start_A + i*matrix1 + j+1),
         *(start_A + i * matrix1 + j));
#endif
*/
            *(start_B + i * matrix1 + j) = ( *(start_A + (i-1) * matrix1 + j) +
                                             *(start_A + (i+1) * matrix1 + j) +
                                             *(start_A + i * matrix1 + (j-1)) +
                                             *(start_A + i * matrix1 + (j+1)) +
                                             2 * *(start_A + i * matrix1 + j)
                                           ) / 6.;
}
}

__global__ void cuda_resid(double *A, double *B, int matrix0, int matrix1, double *red)
{
    int blck_sz[] = { (matrix0 - 2) / gridDim.x + 2, (matrix1 - 2) / gridDim.y + 2};
    int thrd_sz[] = { (blck_sz[0] - 2) / blockDim.x, (blck_sz[1] - 2) / blockDim.y };
    int i_start = (blck_sz[0]-2) * blockIdx.x + thrd_sz[0] * threadIdx.x + 1;
    int j_start = (blck_sz[1]-2) * blockIdx.y + thrd_sz[1] * threadIdx.y + 1;
    double *start_A = A + i_start * matrix1 + j_start;
    double *start_B = B + i_start * matrix1 + j_start;
    double eps = -1.0;
    extern __shared__ double shareps[];
    for(int i = 0; i < thrd_sz[0]; ++i)
        for(int j = 0; j < thrd_sz[1]; ++j)
        {
            eps = fmax(fabs(*(start_A + i * matrix1 + j) - *(start_B + i * matrix1 + j)), eps);
            *(start_A + i * matrix1 + j) = *(start_B + i * matrix1 + j);
        }
    int j = threadIdx.x + threadIdx.y * blockDim.x;
    shareps[j] = eps;
    __syncthreads();

    for(unsigned int i = blockDim.x * blockDim.y / 2; i > 0; i >>= 1)
    {
      if (j < i)
         shareps[j] = fmax(shareps[j],shareps[j + i]);
      __syncthreads();
    }

    if( j == 1 )
       red[ blockIdx.x + blockIdx.y * gridDim.x ] = shareps[0];

}

__global__ void post_reduce(double *src, double *dst)
{
    extern __shared__ double shar[];
    int tid = threadIdx.x;
    int st = blockIdx.x * blockDim.x;
    shar[tid] = src[st+tid];

    __syncthreads();

    for(unsigned int s = blockDim.x / 2; s > 0; s >>= 1)
    {
       if(tid < s)
         shar[tid] = fmax(shar[tid], shar[tid + s]);
       __syncthreads();
    }
    if(tid == 0)
      dst[blockIdx.x] = shar[0];
}

/*
__global__ void cuda_verify()
{

}
*/
