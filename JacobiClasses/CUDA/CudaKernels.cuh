__global__ void cuda_sh_relax(double *A, double *B, int matrix0, int matrix1);
__global__ void cuda_gl_relax(double *A, double *B, int matrix0, int matrix1);
__global__ void cuda_resid(double *A, double *B, int matrix0, int matrix1, double *red);
__global__ void post_reduce(double *src, double *dst);
/*__global__ void cuda_verify(); */
