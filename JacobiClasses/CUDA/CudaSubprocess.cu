#define MPICH_IGNORE_CXX_SEEK 1
#include "CudaSubprocess.h"

#include <iostream>
#include <sstream>

#include <CudaKernels.cuh>

using namespace std;

namespace Jacobi {

CudaSubprocess::CudaSubprocess(const MPI::Cartcomm &comm,
                               const int *this_dims,
                               int big_rank,
                               const int *blocks,
                               const int *cuda_grid,
                               const int *cuda_block,
                               bool use_thrust
                              ) :
    JacobiProcess(comm, this_dims, big_rank, blocks),
    dev_A(NULL), dev_B(NULL),
    dev_UpShape(NULL), dev_RightShape(NULL), dev_LeftShape(NULL), dev_DownShape(NULL),
    UseThrust(use_thrust)
{
    int acc_per_node;
    cudaGetDeviceCount(&acc_per_node);
    cudaSetDevice(big_rank % acc_per_node);
    // Allocating
    // cudaMallocPitch ?
    cudaMalloc(&dev_A, sizeof(double) * BlockSize[0] * BlockSize[1]);
    cudaMalloc(&dev_B, sizeof(double) * BlockSize[0] * BlockSize[1]);
    stringstream x;
    x << "[" << coords[0] << ":" << coords[1] << "] devA:" << dev_A << " devB:" << dev_B << endl;
    cerr << x.str();

    // reallocating shadows in page-locked memory
    JacobiProcess::delShape(UpShape);
    JacobiProcess::delShape(DownShape);
    JacobiProcess::delShape(LeftShape);
    JacobiProcess::delShape(RightShape);

    UpShape = newShape((BlockSize[1] - 2));
    DownShape = newShape((BlockSize[1] - 2));
    LeftShape = newShape((BlockSize[0] - 2));
    RightShape = newShape((BlockSize[0] - 2));

    // Coping matrixes to device
    cudaMemcpy(dev_A,
               A,
               sizeof(double) * BlockSize[0] * BlockSize[1],
               cudaMemcpyHostToDevice
              );
    cudaMemcpy(dev_B,
               B,
               sizeof(double) * BlockSize[0] * BlockSize[1],
               cudaMemcpyHostToDevice
              );

    grid = dim3(cuda_grid[0], cuda_grid[1]);
    block = dim3(cuda_block[0], cuda_block[1]);
}

CudaSubprocess::~CudaSubprocess()
{
    cudaFree(dev_A);
    cudaFree(dev_B);

    delShape(UpShape);
    delShape(DownShape);
    delShape(LeftShape);
    delShape(RightShape);

    UpShape = NULL;
    DownShape = NULL;
    LeftShape = NULL;
    RightShape = NULL;
}

void CudaSubprocess::updateShadowsFromLocal()
{
    cudaError_t err;
    if(up != -1)
    {
        cudaMemcpy(UpShape,
                   dev_A + BlockSize[1] + 1,
                   sizeof(double) * (BlockSize[1] - 2),
                   cudaMemcpyDeviceToHost
                  );
    }
    if(down != -1)
    {
        cudaMemcpy(DownShape,
                   dev_A + BlockSize[1] * (BlockSize[0] - 2) + 1,
                   sizeof(double) * (BlockSize[1] - 2),
                   cudaMemcpyDeviceToHost
                  );
    }
    if(left != -1)
    {
        err =
        cudaMemcpy2D(LeftShape,
                     1 * sizeof(double),
                     dev_A + BlockSize[1] + 1,
                     BlockSize[1] * sizeof(double),
                     1 * sizeof(double),
                     BlockSize[0] - 2,
                     cudaMemcpyDeviceToHost
                    );
        if(err != cudaSuccess) cerr << cudaGetErrorString(err) << endl;
    }
    if(right != -1)
    {
        err =
        cudaMemcpy2D(RightShape,
                     1 * sizeof(double),
                     dev_A + 2 * (BlockSize[1] - 1),
                     BlockSize[1] * sizeof(double),
                     1 * sizeof(double),
                     BlockSize[0] - 2,
                     cudaMemcpyDeviceToHost
                    );
        if(err != cudaSuccess) cerr << cudaGetErrorString(err) << endl;
    }

    Rect.Barrier();
}

void CudaSubprocess::updateLocalFromShadows()
{
    cudaError_t err;
    if(up != -1)
        cudaMemcpy(dev_A + 1,
                   UpShape,
                   sizeof(double) * (BlockSize[1] - 2),
                   cudaMemcpyHostToDevice
                  );
    if(down != -1)
        cudaMemcpy(dev_A + BlockSize[1] * (BlockSize[0] - 1) + 1,
                   DownShape,
                   sizeof(double) * (BlockSize[1] - 2),
                   cudaMemcpyHostToDevice
                  );
    if(left != -1)
    {
        err =
        cudaMemcpy2D(dev_A + BlockSize[1],
                     BlockSize[1] * sizeof(double),
                     LeftShape,
                     1 * sizeof(double),
                     1 * sizeof(double),
                     BlockSize[0] - 2,
                     cudaMemcpyHostToDevice
                    );
        if(err != cudaSuccess) cerr << cudaGetErrorString(err) << endl;
    }
    if(right != -1)
    {
        err =
        cudaMemcpy2D(dev_A + 2 * BlockSize[1] - 1,
                     BlockSize[1] * sizeof(double),
                     RightShape,
                     1 * sizeof(double),
                     1 * sizeof(double),
                     BlockSize[0] - 2,
                     cudaMemcpyHostToDevice
                  );
        if(err != cudaSuccess) cerr << cudaGetErrorString(err) << endl;
    }
    Rect.Barrier();
}

void CudaSubprocess::relax()
{
#ifdef USE_SHARED
    cuda_sh_relax <<< grid, block, ((BlockSize[0]-2)/grid.x + 2) * ((BlockSize[1]-2)/grid.y + 2) * sizeof(double) >>>
                  (dev_A, dev_B, BlockSize[0], BlockSize[1]);
#else
    cuda_gl_relax <<< grid, block >>>
                  (dev_A, dev_B, BlockSize[0], BlockSize[1]);
#endif
}

void CudaSubprocess::resid()
{
    double *eps_arr, *globreduc;
    eps = -1.0;
    cudaMalloc(&eps_arr, sizeof(double) * block.x * block.y);
    cudaMalloc(&globreduc, sizeof(double) * grid.x);

    cudaMemset(&eps_arr, 0, sizeof(double) * block.x * block.y);
    cudaMemset(&globreduc, 0, sizeof(double) * grid.x);

    cuda_resid <<< grid, block, sizeof(double) * block.x * block.y >>>
               (dev_A, dev_B, BlockSize[0], BlockSize[1], eps_arr);
    post_reduce <<< grid.x, grid.y, grid.x * sizeof(double) >>> (eps_arr, globreduc);
    post_reduce <<< 1, grid.x, grid.x * sizeof(double) >>> (globreduc, globreduc);
    cudaMemcpy(&eps, globreduc, sizeof(double), cudaMemcpyDeviceToHost);

}

double CudaSubprocess::verify()
{
    return JacobiProcess::verify();
}

const std::string CudaSubprocess::matrixes()
{
    cudaMemcpy(A, dev_A, (BlockSize[0] * BlockSize[1]) * sizeof(double), cudaMemcpyDeviceToHost);
    cudaMemcpy(B, dev_B, (BlockSize[0] * BlockSize[1]) * sizeof(double), cudaMemcpyDeviceToHost);
    return JacobiProcess::matrixes();
}

double * CudaSubprocess::newShape(size_t size)
{
    double * tmp;
    cudaMallocHost(&tmp, sizeof(double) * size);
    cudaMemset(tmp, -1, sizeof(double) * size);
    return tmp;
}

void CudaSubprocess::delShape(double *shape)
{
    cudaFreeHost(shape);
}

} // namespace Jacobi
