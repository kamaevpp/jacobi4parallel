#ifndef HELPERS_H
#define HELPERS_H

#include <sstream>
#include <iostream>

namespace Jacobi
{

const double maxeps = 0.1e-7;
const int itmax = 100;
const int root_rank = 0;
const int numdims = 2;
const bool periods[] = {false, false};
const bool reorder = false;
//const bool rowdims[] = {false, true}, coldims[] = {true, false};

const int cuda_default_block[] = {16, 16};
const int cuda_default_maxelems_per_thread[] = {2, 2};

inline const std::string format_usage(const std::string& key, const std::string& descript)
{
    std::stringstream x;
    x << key << "\t\t" << descript << std::endl;
    return x.str();
}

inline void usage()
{
    std::cout << "Usage:" << std::endl;
    std::cout << "jacobi [-n <max_proc_num>] "
             #ifdef CUDA_JAC
                 "[+thrust] "
                 "-b <first_dim> <second_dim> "
                 "-ept <first_dim> <second_dim> "
             #endif
                 "N | [-h]" << std::endl;
    std::cout << "MPI parameters:" << std::endl;
    std::cout << format_usage("-h", "Print this message and exit") << std::endl;
    std::cout << format_usage("-n", "Maximum number of MPI process to compute") << std::endl;
    std::cout << format_usage("-g", "MPI processes grid") << std::endl;
    std::cout << "CUDA parameters:" << std::endl;
    std::cout << format_usage("+thrust", "Enable Thrust templates for reduction") << std::endl;
    std::cout << format_usage("-b", "CUDA threads block") << std::endl;
    std::cout << format_usage("-b", "CUDA threads block") << std::endl;
    std::cout << format_usage("-ept", "Size of matrix calculated by one cuda-thread") << std::endl;
}

}
#endif // HELPERS_H
