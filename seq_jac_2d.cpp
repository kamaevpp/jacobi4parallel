#include <cmath>
#include <cstdlib>
#include <sys/time.h>
#include <iostream>

const int N = 8194;
const double maxeps = 0.1e-7;
const int itmax = 100;

double eps;
double A[N][N], B[N][N];

void relax();
void resid();
void init();
void verify(); 

int main(int an, char **as)
{
 struct timeval p_start, p_end;
 double exec_time;
 init();

 gettimeofday(&p_start, 0);
 for(int it=1; it<=itmax; it++)
 {
    eps = 0.;
    relax();
    resid();
    std::cerr << "it = " << it << "\teps = " << eps << std::endl;
    //if (eps < maxeps) break;
 }
 //verify();
 gettimeofday(&p_end, 0);
 exec_time = (p_end.tv_sec - p_start.tv_sec) * 1000000 + p_end.tv_usec - p_start.tv_usec;
 exec_time = exec_time / 1000000;
 std::cout << "Time: " << exec_time << std::endl;
 return 0;
}


void init()
{
  for(int i=0; i<=N-1; i++)
  for(int j=0; j<=N-1; j++)
    {if(i==0 || i==N-1 || j==0 || j==N-1)
         A[i][j] = 0.;
    else A[i][j] = ( 4. + i + j ) ;}
} 


void relax()
{
  for(int i=1; i<=N-2; i++)
  for(int j=1; j<=N-2; j++)
  { 
     B[i][j] = ( A[i-1][j] + A[i+1][j] + A[i][j-1] + A[i][j+1] + A[i][j] + A[i][j] ) / 6.;
  } 
   
}

void resid()
{ 
  for(int i=1; i<=N-2; i++)
  for(int j=1; j<=N-2; j++)
  {
     double e;
     e = fabs(A[i][j] - B[i][j]);         
     A[i][j] = B[i][j]; 
     eps = std::max(eps,e);
  }
}


void verify()
{
  double s;

  s=0.;
  for(int i=0; i<=N-1; i++)
  for(int j=0; j<=N-1; j++)
  {
     s = s + A[i][j]*(i+1)*(j+1)/(N*N);
  }
  std::cout << "\tS = "<< s << std::endl;
}


